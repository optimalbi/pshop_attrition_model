# README #

### What is this repository for? ###

* Quick summary	: EG code managing the data cleaning required prior to testing Powershop data for signs of customer attrition
* Version		: FINAL
* Project Folder: Dropbox (OptimalHq)\Optimal Customers\Powershop\Projects\2015 - Churn Model\Powershop
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

* PLEASE NOTE: this code is written to run off a folder structure as laid out in the Project Folder.  If you try to run it in any other format you will
need to re-point the locations of all sas code


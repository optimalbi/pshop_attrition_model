***************************************************************************************************;
***Program Name: dt_monthend 
***Program Author: Melanie Butler (Data Navigator - OptimalBI)
***Program Client: Powershop
***Program Purpose: prepare month end macros
***Program Process: generate numeric and character monthend dates
***Program Input: enter the number of months back needed for the date macros
***Program Notes:
***************************************************************************************************;
*** generate monthend dates for "x" months back in time;
%macro dt_monthend(x);
%global monthsrt monthend mondate;
data _null_;
	monthsrt = intnx('month',today(),-&x,'b');
	monthend = intnx('month',today(),-&x,'e');
 	call symputx('monthsrt', cat("'",put(monthsrt,date9.),"'d"));
	call symputx('monthend', cat("'",put(monthend,date9.),"'d"));
	call symputx('mondate', put(monthend,monyy.));
run;
*readout macros for easy reminding access ;
%put monthsrt is &monthsrt;
%put monthend is &monthend;
%put mondate is &mondate;
%mend;
%dt_monthend(1);
***************************************************************************************************;
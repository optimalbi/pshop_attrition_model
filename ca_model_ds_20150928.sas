***************************************************************************************************;
***Program Name: ca_model_ds_20150928
***Program Author: Melanie Butler (Data Navigator - OptimalBI)
***Program Client: Powershop
***Program Purpose: collate identifying information for use in EM to predict churn (and present probably churners)

***Program Process:
01. import input data sets
02. calculate average tenure of customer - used to calculate worth i.e., $rev x X(t) = worth
03. summarise annual revenue per customer - use to calculate worth of customer - then by variation variable in 5
04. look at voluntary versus involuntary (delq) attrition
05. lapsed returns
06. time to next purchase

***Program Input:
1. sas dataset provided - purchases - all purchases ever
2. pricetrend.xls - competitor pricing
3. allcust.xls - all Powershop customers ever
4. JulyPurchase.xls - July 2015 only purchases
5. stypelosses - customers who have moved

***Program Output:

***Program Notes:
Assumptions:
1. 	if a customer has been observed to leave they are called observed customers and status = 1
2. 	if a customer has not left they are called resident customers and status = 0 
3. 	a duration of tenure is calculated using the start date of the relationship until the end date
	of the relationship for observed customers.  
4. 	For resident customers the duration of tenure is calculated using the start data of the relationship
	until the end of the reporting month (calculated using %dt_monthend macro)
5. 	Powershop ideally want to keep customers that have already reached a tenure of three years
6.  if a customer does not have a start date will not consider them a "real" customer for the purpose
	of attrition modelling

***************************************************************************************************;
*** set library;
libname pshop "E:\Powershop\sasdata";

*** set macros;
%let psdata = E:\Powershop\input; /* file locations */
%let outdata = E:\Powershop\output; /* file output locations */
%let custten = 90; /* contract extension allowance period - checked with Chris Prout */
%include "E:\Powershop\sasmacro\dt_monthend.sas"; /* previous month date macros */
%dt_monthend(1);

***************************************************************************************************;
*** import powershop data;
*** only needs to be run once - however will not cause any issues unless the underlying data changes ;
*** 1. all customers - all customers ever;
proc import datafile="&psdata.\AllCust150902.csv"
	out=pshop.allcust replace;
	/*sheet="sheet1";*/
run;

*** 2. stypelosses - known information about customers who have left;
proc import datafile="&psdata.\STypeLosses150831.csv"
	out=pshop.stlosses replace;
	/*sheet="sheet1";*/
run;

*** 3. PriceTrend - competitor information;
proc import datafile="&psdata.\PriceTrend_20150901.csv"
	out=pshop.pricetrend replace;
	/*sheet="sheet1";*/
run;

*** 4. customer movement detail - known information about customers movements either off or between installations (homes);
proc import datafile="&psdata.\customer_movement_detail_1508.csv"
	out=pshop.cust_move replace;
	/*sheet="sheet1";*/
run;

*** 5. stypecodes - competitor acronym mappings;
proc import datafile="&psdata.\STypeCodes150831.csv"
	out=pshop.stcodes replace;
	/*sheet="sheet1";*/
run;

*** 6. contact centre calls - overview information from the contact centre;
data work.calls;
%let _EFIERR_ = 0; /* set the ERROR detection macro variable */
infile "&psdata.\Calls150925.txt" dlm='09'x flowover scanover truncover DSD lrecl=32767 firstobs=2 termstr=crlf ;
	informat Int_ID $6. ;
    informat ticket_id $6. ;
    informat Customer_Name $36. ;
    informat Cust_Created $21. ;
    informat interaction_type $15. ;
    informat business_action $15. ;
    informat direction $15. ;
    informat subject $25. ;
    informat Int_Created_At $21. ;
    informat Int_Created_By $17. ;
    informat Tkt_Created_At $21. ;
    informat Assigned_To $17. ;
	informat Tkt_Type $17. ;
    informat Tkt_Category $50. ;
    informat Review_Required $4. ;
    informat action_required_by $1. ;
    informat Tkt_Actioned_By $17. ;
    informat actioned_at $21. ;
    informat cti_session_id $1. ;
    informat start_mood $1. ;
    informat end_mood $1. ;
    informat tkt_email $28. ;
    informat user_email $28. ;
    informat customer_number $10. ;
    informat Int_Body $280. ;

    format Int_ID $6. ;
    format ticket_id $6. ;
    format Customer_Name $36. ;
    format Cust_Created $21. ;
    format interaction_type $15. ;
    format business_action $15. ;
    format direction $15. ;
    format subject $25. ;
    format Int_Created_At $21. ;
	format Int_Created_By $17. ;
	format Tkt_Created_At $21. ;
	format Assigned_To $17. ;
	format Tkt_Type $17. ;
	format Tkt_Category $50. ;
	format Review_Required $4. ;
	format action_required_by $1. ;
	format Tkt_Actioned_By $17. ;
	format actioned_at $21. ;
	format cti_session_id $1. ;
	format start_mood $1. ;
	format end_mood $1. ;
	format tkt_email $28. ;
	format user_email $28. ;
	format customer_number $10. ;
	format Int_Body $280. ;

    input
		Int_ID $
		ticket_id $
		Customer_Name $
		Cust_Created $
		interaction_type $
		business_action $
		direction $
		subject $
		Int_Created_At $
		Int_Created_By $
		Tkt_Created_At $
		Assigned_To $
		Tkt_Type $
		Tkt_Category $
		Review_Required $
		action_required_by $
		Tkt_Actioned_By $
		actioned_at $
		cti_session_id $
		start_mood $
		end_mood $
		tkt_email $
		user_email $
		customer_number $
		Int_Body $ ////
		;

	if _ERROR_ then call symputx('_EFIERR_',1);  /* set ERROR detection macro variable */
run;
/*** data is a bit nasty - for speed will not try to tidy up the carriage returns ***/
proc sql;
create table pshop.calls as
select
	*
from
	work.calls
where
	direction in ("to_customer","to_powershop")
;
quit;

***************************************************************************************************;
*** create initial dataset (ds) with as much demographic information as available ;
*** there is no demographic information provided - add if and when ;

***************************************************************************************************;
*** create initial dataset (ds) with as much relationship origination information as available ;
data work.ps_origination(keep=	custID 
								ConsID 
								id
								start_date	
								end_date
								dur 
								switching_plan 
								account_type 
								SmartMetered 
								annual_consumption_accepted 
								estimated_units_per_year
								status
								usage_acc
								keeper
								);
set pshop.allcust;

/*** set missing end_dates to end of reporting month and
	 assign observed status ***/	
if end_date = ''d then do;
	if start_date <> ''d then do;
		end_date = &monthend.;
		end;
	status = 0;
	end;
else do;
	status = 1;
	end;

/*** calculate the duration of tenure ***/
dur = intck('month',start_date,end_date);

/*** denote the keepers - anyone with a tenure >36 months is a keeper ***/
if dur gt 36 then do;
	keeper = 1;
	end;
else do;
	keeper = 0;
	end;

/*** calculate the accuracy of the customers original power estimation ***/
usage_acc = annual_consumption_accepted/estimated_units_per_year;

/*** if a customer doesn't have a start_date not considering them a "real" customer
	 so exclude them from this ds ***/
if start_date <> ''d then output;

run;

*** merge into Powershop full modelling dataset ***;
data pshop.powershop_model_ds;
set work.ps_origination;
run;

proc sort data=pshop.powershop_model_ds;
	by custid start_date;
run;

***************************************************************************************************;
*** create initial dataset (ds) with as much customer movement information as available ;
data work.ps_cust_move(drop=cus_start_date	
							cus_end_date
							led
							);
set pshop.cust_move	(keep=	customer_id
							cus_start_date
							cus_end_date
							cus_start_season
							cus_end_season
							cus_current
							cus_bill_day_sync
							cus_staff
							cus_installs_num_ever
							cus_installs_num_current 
							);

by customer_id;

/*** convert numeric date into date9. ***/
c_start_dt = input(put(cus_start_date,8.),yymmdd10.);
c_end_dt = input(put(cus_end_date,8.),yymmdd10.);
format c_start_dt c_end_dt led date9.;

/*** set missing end_dates to end of reporting month and
	 assign observed status ***/	
if c_end_dt = ''d then do;
	c_end_dt = &monthend.;
	end;

led = lag(c_end_dt); /*** for later use in calculating time to return ***/

if first.customer_id and last.customer_id then do;
		return = 0;
		led = .;
		end;
	else if first.customer_id then do;
		return = 1;
		led = .;
		end;
	else do;
		return = 2;
		end;

/*** calculate time to return ***/
ttr = intck('month',led,c_start_dt);

run;

*** separate into single and multiple relationship customers ;
data work.ps_single work.ps_dup;
set work.ps_cust_move;
by customer_id;
if first.customer_id and last.customer_id then output work.ps_single;
else output work.ps_dup;
run;

*** merge the single relationship customers into the Powershop full modelling ds ;
*** some of the customer movement customers do not exist in the all customers data
	must be out of sync - therefore excluding the rows causing this data quality issue
	as minimal in value - will be included once the all customers data is more up-to-date ;
data work.ps_add_single;
merge	pshop.powershop_model_ds(in=a)
		work.ps_single(in=b rename=(customer_id=custid c_start_dt=start_date));
if a;
by custid start_date;
run;

*** merge the duplicate relationship customers into the Powershop full modelling ds ;
*** some duplicate relationship customers have movement information that spans more than one
	installation (home) so repeat movement information over the two all customer rows;
*** the majority of customers have the same number of movement rows as the number of
	installations - so add them to the Powershop full modelling ds first ;
data 	work.ps_dup_simple(keep=customer_id) 
		work.ps_dup_complex(keep=customer_id);
set	work.ps_dup;
by customer_id;
if last.customer_id then do;
	if cus_installs_num_ever = return then output work.ps_dup_simple;
	else output work.ps_dup_complex;
	end;
run;

*** extract all rows for customers with the same number of all customer rows
	as customer movements - will check for the start dates in the merge
	to ensure data correctly aligned ;
proc sql;
create table work.ps_simple_ds as
select
	b.*
from
	work.ps_dup_simple a,
	work.ps_dup b
where
	a.customer_id = b.customer_id
;
quit;

*** merge the simple duplicate relationship customers into the Powershop full modelling ds ;
*** checked to ensure that 5,864 rows (as above) were added with no extra rows added or lost ;
data work.ps_add_simple;
merge	work.ps_add_single(in=a)
		work.ps_simple_ds(in=b rename=(customer_id=custid c_start_dt=start_date));
if a;
by custid start_date;
run;
proc sort data=work.ps_add_simple;
	by custid consid start_date end_date switching_plan account_type id;
run;

***************************************************************************************************;
*** extract all rows for customers with the different all customer rows as customer movements - will 
	check for the start dates in the merge to ensure data correctly aligned ;
proc sql;
create table work.ps_complex_ds as
select
	b.*
from
	work.ps_dup_complex a,
	work.ps_dup b
where
	a.customer_id = b.customer_id
;
quit;

*** merge movement data into all customer data to align movement information;
*** my understanding after having spoken to Chris is that is a customer has multi installations
	(namely rental properties et al) and their contract falls within the overarching time span
	of the primary installation - then all of the installations fall into the same customer
	movement row - so will align that customer movement row with each contained installation
	by start and end date ;
proc sql;
create table work.ps_work_complex_a as
select
	a.custid,
	a.consid,
	a.start_date,
	b.c_start_dt,
	a.end_date,
	b.c_end_dt,
	a.switching_plan,
	a.account_type,
	a.id,
	a.SmartMetered,
	a.annual_consumption_accepted,
	a.estimated_units_per_year,
	a.status,
	a.usage_acc,
	b.cus_start_season,
	b.cus_end_season,
	b.cus_current,
	b.cus_bill_day_sync,
	b.cus_staff,
	b.cus_installs_num_ever,
	b.cus_installs_num_current,
	a.keeper,
	b.return,
	b.ttr
from
	work.ps_add_simple a,
	work.ps_complex_ds b
where
/*** first look for absolute start and end date matches for a customer ***/
	a.custid = b.customer_id
and a.start_date = b.c_start_dt
and a.end_date = b.c_end_dt
order by
	a.custid, a.consid, a.start_date, a.end_date, a.switching_plan, a.account_type, a.id
;
quit;

/*** update work.ps_add_simple with the first matched complex customer movement information ***/
data work.ps_add_a;
	update work.ps_add_simple work.ps_work_complex_a updatemode=nomissingcheck;
	by custid consid start_date end_date switching_plan account_type id;
run;

/*** don't want to match the same row twice so exclude rows already matched in above step ***/
proc sql;
create table work.complex_to_match_a as	
select
	a.*
from 
	work.ps_add_simple a,
	(
	select 
		a.custid,
		a.consid,
		a.start_date,
		a.end_date,
		a.switching_plan,
		a.account_type,
		a.id
	from
		work.ps_add_simple a,
		work.ps_dup_complex b
	where 
		a.custid = b.customer_id
	except
	select 
		c.custid,
		c.consid,
		c.start_date,
		c.end_date,
		c.switching_plan,
		c.account_type,
		c.id
	from
		work.ps_work_complex_a c
	) b
where 
	a.custid = b.custid
and	a.consid = b.consid
and	a.start_date = b.start_date
and	a.end_date = b.end_date
and	a.switching_plan = b.switching_plan
and	a.account_type = b.account_type
and	a.id = b.id
;
quit;

*** second look for situations where the start date is the same but the end_date is earlier ;
proc sql;
create table work.ps_work_complex_b as
select
	a.custid,
	a.consid,
	a.start_date,
	b.c_start_dt,
	a.end_date,
	b.c_end_dt,
	a.switching_plan,
	a.account_type,
	a.id,
	a.SmartMetered,
	a.annual_consumption_accepted,
	a.estimated_units_per_year,
	a.status,
	a.usage_acc,
	b.cus_start_season,
	b.cus_end_season,
	b.cus_current,
	b.cus_bill_day_sync,
	b.cus_staff,
	b.cus_installs_num_ever,
	b.cus_installs_num_current,
	a.keeper,
	b.return,
	b.ttr
from
	work.complex_to_match_a a,
	work.ps_complex_ds b
where
/*** look for equal start date and an earlier customer movement end date for a customer ***/
	a.custid = b.customer_id
and a.start_date = b.c_start_dt
and a.end_date le b.c_end_dt
order by
	a.custid, a.consid, a.start_date, a.end_date, a.switching_plan, a.account_type, a.id
;
quit;

/*** update work.ps_add_a with the second matched complex customer movement information ***/
data work.ps_add_b;
	update work.ps_add_a work.ps_work_complex_b updatemode=nomissingcheck;
	by custid consid start_date end_date switching_plan account_type id;
run;

/*** don't want to match the same row twice so exclude rows already matched in above step (B) ***/
proc sql;
create table work.complex_to_match_b as	
select
	a.*
from 
	work.complex_to_match_a a,
	(
	select 
		a.custid,
		a.consid,
		a.start_date,
		a.end_date,
		a.switching_plan,
		a.account_type,
		a.id
	from
		work.complex_to_match_a a,
		work.ps_dup_complex b
	where 
		a.custid = b.customer_id
	except
	select 
		c.custid,
		c.consid,
		c.start_date,
		c.end_date,
		c.switching_plan,
		c.account_type,
		c.id
	from
		work.ps_work_complex_b c
	) b
where 
	a.custid = b.custid
and	a.consid = b.consid
and	a.start_date = b.start_date
and	a.end_date = b.end_date
and	a.switching_plan = b.switching_plan
and	a.account_type = b.account_type
and	a.id = b.id
;
quit;

*** third look for situations where the start date is later but the end_date matches ;
proc sql;
create table work.ps_work_complex_c as
select
	a.custid,
	a.consid,
	a.start_date,
	b.c_start_dt,
	a.end_date,
	b.c_end_dt,
	a.switching_plan,
	a.account_type,
	a.id,
	a.SmartMetered,
	a.annual_consumption_accepted,
	a.estimated_units_per_year,
	a.status,
	a.usage_acc,
	b.cus_start_season,
	b.cus_end_season,
	b.cus_current,
	b.cus_bill_day_sync,
	b.cus_staff,
	b.cus_installs_num_ever,
	b.cus_installs_num_current,
	a.keeper,
	b.return,
	b.ttr
from
	work.complex_to_match_b a,
	work.ps_complex_ds b
where
/*** look for later start date and a matching customer movement end date for a customer ***/
	a.custid = b.customer_id
and a.start_date ge b.c_start_dt
and a.end_date = b.c_end_dt
order by
	a.custid, a.consid, a.start_date, a.end_date, a.switching_plan, a.account_type, a.id
;
quit;

/*** update work.ps_add_b with the third matched complex customer movement information ***/
data work.ps_add_c;
	update work.ps_add_b work.ps_work_complex_c updatemode=nomissingcheck;
	by custid consid start_date end_date switching_plan account_type id;
run;

/*** don't want to match the same row twice so exclude rows already matched in above step (C) ***/
proc sql;
create table work.complex_to_match_c as	
select
	a.*
from 
	work.complex_to_match_b a,
	(
	select 
		a.custid,
		a.consid,
		a.start_date,
		a.end_date,
		a.switching_plan,
		a.account_type,
		a.id
	from
		work.complex_to_match_b a,
		work.ps_dup_complex b
	where 
		a.custid = b.customer_id
	except
	select 
		c.custid,
		c.consid,
		c.start_date,
		c.end_date,
		c.switching_plan,
		c.account_type,
		c.id
	from
		work.ps_work_complex_c c
	) b
where 
	a.custid = b.custid
and	a.consid = b.consid
and	a.start_date = b.start_date
and	a.end_date = b.end_date
and	a.switching_plan = b.switching_plan
and	a.account_type = b.account_type
and	a.id = b.id
;
quit;

*** last look for situations where the start date and end_date are included in the
	time span given in the customer movement data ;
proc sql;
create table work.ps_work_complex_d as
select
	a.custid,
	a.consid,
	a.start_date,
	b.c_start_dt,
	a.end_date,
	b.c_end_dt,
	a.switching_plan,
	a.account_type,
	a.id,
	a.SmartMetered,
	a.annual_consumption_accepted,
	a.estimated_units_per_year,
	a.status,
	a.usage_acc,
	b.cus_start_season,
	b.cus_end_season,
	b.cus_current,
	b.cus_bill_day_sync,
	b.cus_staff,
	b.cus_installs_num_ever,
	b.cus_installs_num_current,
	a.keeper,
	b.return,
	b.ttr
from
	work.complex_to_match_c a,
	work.ps_complex_ds b
where
/*** look for later start date and an earlier customer movement end date for a customer ***/
	a.custid = b.customer_id
and a.start_date ge b.c_start_dt
and a.end_date le b.c_end_dt
order by
	a.custid, a.consid, a.start_date, a.end_date, a.switching_plan, a.account_type, a.id
;
quit;

/*** update work.ps_add_c with the fourth matched complex customer movement information ***/
data work.ps_add_d;
	update work.ps_add_c work.ps_work_complex_d updatemode=nomissingcheck;
	by custid consid start_date end_date switching_plan account_type id;
run;

/*** don't want to match the same row twice so exclude rows already matched in above step (D) ***/
/*** there are 5 customers who don't fit into any of the above situations - one has a forward dated
	 end date - the other 4 are new returning customers - reappeared in August data is from September ***/
proc sql;
create table work.complex_to_match_d as	
select
	a.*
from 
	work.complex_to_match_c a,
	(
	select 
		a.custid,
		a.consid,
		a.start_date,
		a.end_date,
		a.switching_plan,
		a.account_type,
		a.id
	from
		work.complex_to_match_c a,
		work.ps_dup_complex b
	where 
		a.custid = b.customer_id
	except
	select 
		c.custid,
		c.consid,
		c.start_date,
		c.end_date,
		c.switching_plan,
		c.account_type,
		c.id
	from
		work.ps_work_complex_d c
	) b
where 
	a.custid = b.custid
and	a.consid = b.consid
and	a.start_date = b.start_date
and	a.end_date = b.end_date
and	a.switching_plan = b.switching_plan
and	a.account_type = b.account_type
and	a.id = b.id
;
quit;

/*** want to look at all the rows for these 5 customers ***/
/*** have left all dates so I can compare them in case they can be further included 
	 they cannot ***/
proc sql;
create table work.ps_letsee_complex as
select
	a.custid,
	a.consid,
	a.start_date,
	b.c_start_dt,
	a.end_date,
	b.c_end_dt,
	a.switching_plan,
	a.account_type,
	a.id,
	a.SmartMetered,
	a.annual_consumption_accepted,
	a.estimated_units_per_year,
	a.status,
	a.usage_acc,
	b.cus_start_season,
	b.cus_end_season,
	b.cus_current,
	b.cus_bill_day_sync,
	b.cus_staff,
	b.cus_installs_num_ever,
	b.cus_installs_num_current,
	a.keeper,
	b.return,
	b.ttr
from
	work.ps_add_simple a,
	work.letsee b
where
	a.custid = b.customer_id
;
quit;

***************************************************************************************************;
*** work.ps_add_d is now the up-to-date powershop model ds ;
data pshop.powershop_model_ds(drop=c_end_dt c_start_dt);
	set work.ps_add_d;
run;

***************************************************************************************************;
*** categorise the contact centre information into summary points for the model ;
data work.letsee_calls(drop=start_mood
							end_mood);
retain custid customer_number direction interaction_type tkt_type tkt_category tkt_create_dt start_mood_a end_mood_a;
set pshop.calls(keep=	customer_number
						direction
						interaction_type
						tkt_type
						tkt_category
						tkt_created_at
						start_mood
						end_mood
						);

custid=substrn(customer_number,2,7)*1;
start_mood_a=start_mood*1;
end_mood_a=end_mood*1;

tkt_created_dt = datepart(input(tkt_created_at,anydtdtm.));
format tkt_created_dt date9.;

if custid > 0 then output;

run;
proc sort data=work.letsee_calls;
	by custid tkt_created_dt;
run;

*** want to look at calls made 90 days prior to ending the customer relationship or
	3 months prior to the end of the reporting month ;
proc sql;
create table work.cust_who_called as
select
	a.*,
	b.end_date
from
	work.letsee_calls a,
	pshop.powershop_model_ds b
where
	a.custid = b.custid
and a.tkt_created_dt between intnx('day',b.end_date,-&custten) and b.end_date
;
quit;

*** summarise calls by the tkt_category, start_mood and end_mood ;
proc summary data=work.cust_who_called nway missing;
class customer_number end_date tkt_category;
var custid start_mood_a end_mood_a;
output out=work.cust_called_sum (drop=_type_ _freq_)
n(custid) = total
mean(start_mood_a) = call_start_mood
mean(end_mood_a) = call_end_mood
;
run;

*** summarise calls by the interaction_type ;
proc summary data=work.cust_who_called nway missing;
class customer_number end_date interaction_type ;
var custid ;
output out=work.cust_called_sumtwo (drop=_type_ _freq_)
n(custid) = total
;
run;

*** summarise calls by the tkt_type ;
proc summary data=work.cust_who_called nway missing;
class customer_number end_date tkt_type;
var custid ;
output out=work.cust_called_sumthr (drop=_type_ _freq_)
n(custid) = total
;
run;

*** prepare contact centre call mood information by date ;
data work.call_mood;
retain custid customer_number end_date call_start_mood call_end_mood;
set work.cust_called_sum(keep =	customer_number
								end_date
								call_start_mood
								call_end_mood
								);

custid=substrn(customer_number,2,7)*1;
new_id=cat(customer_number,end_date);
run;
proc sort data=work.call_mood;
	by new_id call_start_mood call_end_mood;
run;

/*** extract mood ratings for calls on a given day for a given customer ***/
data work.call_mood_final(drop =new_id
								customer_number);
set work.call_mood;
by new_id;
if last.new_id then output;
run;

/*** add mood information to the powership model ds ***/
proc sort data=pshop.powershop_model_ds;by custid end_date;run;
data pshop.powershop_model_ds;
merge	pshop.powershop_model_ds(in=a)
		work.call_mood_final(in=b);
if a;
by custid end_date;
run;

***************************************************************************************************;
*** work on the tkt_category to align spelling and assumed meaning ;
data work.cust_called_altered;
set work.cust_called_sum;
format category $35.;
if tkt_category in ('Field services','Field Services') then category = 'Field Services';
else if tkt_category in ('Recharge card','Recharge Card') then category = 'Recharge Card';
else category=tkt_category;
run;

*** transpose contact centre categories for the model ds ;
*** there is very little weight at this level so will go one level up until the contact
	centre calls are found to be meaningful ;
proc sort data=work.cust_called_altered;by customer_number end_date category;run;
proc transpose data=work.cust_called_altered(drop =	call_end_mood
													call_start_mood)
				out=work.cust_called_trans(drop=_name_);
by customer_number end_date;
id category;
var total;
run;

*** transpose high level contact centre categories for the model ds ;
*** first the interaction type ;
proc sort data=work.cust_called_sumtwo;by customer_number end_date interaction_type;run;
proc transpose 	data=work.cust_called_sumtwo
				out=work.cust_called_transtwo(drop=_name_);
by customer_number end_date;
id interaction_type;
var total;
run;

/*** add interaction_type information to the powership model ds ***/
proc sql;
create table work.cust_called_two as
select
	input(substrn(customer_number,2,7),7.) as custid,
	*
from
	work.cust_called_transtwo
;
quit;
proc sort data=pshop.powershop_model_ds;by custid end_date;run;
data pshop.powershop_model_ds;
merge	pshop.powershop_model_ds(in=a)
		work.cust_called_two(in=b drop=customer_number);
if a;
by custid end_date;
run;
		
*** first the interaction type ;
proc sort data=work.cust_called_sumthr;by customer_number end_date tkt_type;run;
proc transpose 	data=work.cust_called_sumthr
				out=work.cust_called_transthr(drop=_name_);
by customer_number end_date;
id tkt_type;
var total;
run;

/*** add tkt_type information to the powership model ds ***/
proc sql;
create table work.cust_called_thr as
select
	input(substrn(customer_number,2,7),7.) as custid,
	*
from
	work.cust_called_transthr
;
quit;
proc sort data=pshop.powershop_model_ds;by custid end_date;run;
/*** merges the intereaction_type feedback over the tkt_type feedback ***/
data pshop.powershop_model_ds;
merge	pshop.powershop_model_ds(in=a)
		work.cust_called_thr(in=b drop=customer_number);
if a;
by custid end_date;
run;
*** try model at this point - if need more info add delinquency and then competitor information ;
/*** after talking to Chris will now look at recent attritors - customers who were customers
	 within the last year both those who have left and those who have joined or stayed ***/
data pshop.powershop_sample;
set pshop.powershop_model_ds;
where end_date ge intnx('month',&monthend.,-12);
run;
/*** plus extract a score sample for the final prediction of attrition ***/
data pshop.powershop_score;
set pshop.powershop_model_ds;
where end_date = &monthend.;
run;
***************************************************************************************************;
/*** check out the resulting score data ***/
libname emws1 "E:\Powershop\project\PS_Attrition_Model\Workspaces\EMWS1";
/*** sort resulting dataset by p_score ***/
proc sort data=emws1.score_score out=work.letsort;
by descending p_status1;
run;
/*** look at the list = top rows being those more likely to attrite ***/
data work.letsee_business work.letsee_residential;
set work.letsort;
if account_type = "business" then output work.letsee_business;
else output work.letsee_residential;
run;
/*** export residential probably attritors ***/
data pshop.powershop_probables;
set work.letsee_residential;
keep custid annual_consumption_accepted keeper p_status1;
run;
***************************************************************************************************;
*** check some customers that the model predicts will attrite ;
proc sql;
create table work.check_cust as
select
	*
from
	work.letsee_residential /*pshop.powershop_model_ds*/
where
	custid in (97528,652,16194,70697,39060,4844,34007,30540,27415)
and end_date = &monthend.
;
quit;
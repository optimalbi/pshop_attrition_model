***************************************************************************************************;
***Program Name: ca_attrite_retain_20150928
***Program Author: Melanie Butler (Data Navigator - OptimalBI)
***Program Client: Powershop
***Program Purpose: look at the probability of attrition and retention of Powershop customers

***Program Process:
01. import input data sets
02. calculate the E(attrite @t) = probability of attrition
03. graph - look for noticeable dips - look for loyalty - align to known time based Powershop business practice
04. calculate the E(retain @t) = 1-probability of attrition
05. graph - look for variance between payment methodology et al
06. calculate median tenure of customer  - place where you expect half you customer based to still be active

***Program Input:
1. sas dataset provided - purchases - all purchases ever
2. pricetrend.xls - competitor pricing
3. allcust.xls - all Powershop customers ever
4. JulyPurchase.xls - July 2015 only purchases
5. stypelosses - customers who have moved

***Program Output:
1. E:\Powershop\output\leaving.csv - table of p(hazard) figures - leaving
2. E:\Powershop\output\staying.csv - table of p(survival) figures - staying
3. E:\Powershop\output\full_med_mean.csv - table of medians and means for the whole population
4. E:\Powershop\output\sub_med_mean.csv - table of medians and means for population by switching plan

***Program Notes:
Assumptions: 
1. if a customer has not attrited their end date is set to today()
2. use binary to denote attrite or not - 1 = yes

***************************************************************************************************;
*** set library;
libname pshop "E:\Powershop\sasdata";

*** set macros;
%let psdata = E:\Powershop\input; /* file locations */
%let outdata = E:\Powershop\output; /* file output locations */
%let custten = 20; /* contract extension allowance period */
%include "E:\Powershop\sasmacro\dt_monthend.sas"; /* previous month date macros */
%dt_monthend(1);

***************************************************************************************************;
*** import powershop data;
*** 1. all customers;
proc import datafile="&psdata.\AllCust150902.csv"
	out=pshop.allcust replace;
	/*sheet="sheet1";*/
run;

*** 2. stypelosses;
proc import datafile="&psdata.\STypeLosses150831.csv"
	out=pshop.stlosses replace;
	/*sheet="sheet1";*/
run;

*** 3. PriceTrend;
proc import datafile="&psdata.\PriceTrend_20150901.csv"
	out=pshop.pricetrend replace;
	/*sheet="sheet1";*/
run;

*** 4. customer movement detail;
proc import datafile="&psdata.\customer_movement_detail_1508.csv"
	out=pshop.cust_move replace;
	/*sheet="sheet1";*/
run;

*** 5. stypecodes;
proc import datafile="&psdata.\STypeCodes150831.csv"
	out=pshop.stcodes replace;
	/*sheet="sheet1";*/
run;

***************************************************************************************************;
*** calculate the probability of attriting;
***************************************************************************************************;
*** normalise the addresses;
*** there are a number of data entry issues with the address data so I am compressing the field to remove
	spaces and then standardising the field by converting text to upper case
	I am also reducing the number of characters to allow for various ways to spell st/street et al
	this is not fool proof but will help try to follow longer tenure customers at the same address as
	well as trying to identify people who have moved and transfered within &custten days ;
data pshop.allcust_mod ;
set pshop.allcust ;

where start_date ne .;

new_addr=substrn(compress(upcase(addr)),1,7);
sw_plan=compress(upcase(switching_plan));
unique_cust=cat(custid,consid,new_addr);

/* where contract is active set end_date to the end of the month */
if end_date = . then end_date = &monthend.;
/* calculate the actual tenure of the customer */
tenure = intck('month',start_date,end_date);

run;
***************************************************************************************************;
*** prepare customer start and end dates in chronological order 
	including the lapsed return of customers;
proc sort	data=pshop.allcust_mod (keep=unique_cust custID ConsID sw_plan start_date end_date new_addr
			/* where=(custid=27531)*/)
			out=pshop.raw_cust_tenure;
			by unique_cust start_date end_date;
run;

*** look at the number of switching plans - including manually - automate if needed ;
proc summary data=pshop.raw_cust_tenure nway missing;
class sw_plan;
var custid;
output out=work.sw_plan_count (drop=_type_ _freq_) n=;
run;

*** calculate the maximum number of occurences of one unique customer blend in this dataset;
*** until have some rules to identify moving and/or returning customers this will be inaccurate ;
proc sql;
select
	max(number_occ) 
into :maxocc
from
	(
	select 
		unique_cust,
		count(unique_cust) as number_occ
	from 
		pshop.raw_cust_tenure
	group by unique_cust
	)
;	
%let maxocc=&maxocc;
%put NOTE: maximum of &maxocc recurrences for one customer ; 

*** determine the full tenure of customers Powershop relationship;
*** includes moving house and re-signing to new contract within the tenure of the previous contract;
*** assume anyone who moves within one month (either way) is a retained customer ;
*** treat single contract customer differently to multiple contract customers so create two datasets
	one for singles and one for multis ;
data 	pshop.cust_single(keep=custid consid unique_cust sw_plan a_sw_plan b_sw_plan co_sw_plan fa_sw_plan 
								sm_sw_plan no_sw_plan start_work end_work change) 
		pshop.cust_multi;
set pshop.raw_cust_tenure;

by unique_cust start_date;

format start_work end_work date9.;
format a_sw_plan b_sw_plan co_sw_plan fa_sw_plan sm_sw_plan no_sw_plan date9.;
format change $15.;

/* assign start dates to switching plans */
/* assumes that the switching plan started on the start date */
if sw_plan = "A" then do;a_sw_plan = start_date;end;
else if sw_plan = "B" then do; b_sw_plan = start_date;end;
else if sw_plan = "CO" then do; co_sw_plan = start_date;end;
else if sw_plan = "FA" then do; fa_sw_plan = start_date;end;
else if sw_plan = "SM" then do; sm_sw_plan = start_date;end;
else no_sw_plan = start_date;

/* set full contract dates to the working dates and output all single contract customers */
if first.unique_cust and last.unique_cust then do;
	start_work=start_date;
	end_work=end_date;
	change = "new";
 	
	output pshop.cust_single ;
	end;

/* segregate multipe contract customers will run through these separately */
else output pshop.cust_multi;

run;

*** look at multiple contract customers to decide if they have sequential contracts or whether
	they are lapsed return customers (or rentals ??) ;
*** PLEASE NOTE: some customers may appear to be parallel tenants - treating these as singles ;
data 	work.look_at_multis 
		pshop.multi_output(drop=new_addr start_date end_date) ;
set pshop.cust_multi ;
by unique_cust start_date;

retain old_a_sw_plan old_b_sw_plan old_co_sw_plan old_fa_sw_plan old_sm_sw_plan old_no_sw_plan;

format start_work end_work date9.;

start_work = lag(start_date);
end_work = lag(end_date);

if first.unique_cust then do;
	start_work = start_date;
	end_work=.;
	change = "new";
	old_a_sw_plan = a_sw_plan;
	old_b_sw_plan = b_sw_plan;
	old_co_sw_plan = co_sw_plan;
	old_fa_sw_plan = fa_sw_plan;
	old_sm_sw_plan = sm_sw_plan;
	old_no_sw_plan = no_sw_plan;
	end;

if start_date-&custten. le end_work le start_date+&custten. then do;
	change = "re-sign/sold";
	end_work = end_date;
/* collate switch plan A start date */
	if old_a_sw_plan ne . then do;
		if a_sw_plan ne . then do;
			if old_a_sw_plan lt a_sw_plan then a_sw_plan=old_a_sw_plan;end;
		else a_sw_plan=old_a_sw_plan;end;

/* collate switch plan B start date */
	else if old_b_sw_plan ne . then do;
		if b_sw_plan ne . then do;
			if old_b_sw_plan lt b_sw_plan then b_sw_plan=old_b_sw_plan;end;
		else b_sw_plan=old_b_sw_plan;end;

/* collate switch plan CO start date */
	else if old_co_sw_plan ne . then do;
		if co_sw_plan ne . then do;
			if old_co_sw_plan lt co_sw_plan then co_sw_plan=old_co_sw_plan;end;
		else co_sw_plan=old_co_sw_plan;end;

/* collate switch plan FA start date */
	else if old_fa_sw_plan ne . then do;
		if fa_sw_plan ne . then do;
			if old_fa_sw_plan lt fa_sw_plan then fa_sw_plan=old_fa_sw_plan;end;
		else fa_sw_plan=old_fa_sw_plan;end;

/* collate switch plan SM start date */
	else if old_sm_sw_plan ne . then do;
		if sm_sw_plan ne . then do;
			if old_sm_sw_plan lt sm_sw_plan then sm_sw_plan=old_sm_sw_plan;end;
		else sm_sw_plan=old_sm_sw_plan;end;

/* collate switch plan NO start date */
	else if old_no_sw_plan ne . then do;
		if no_sw_plan ne . then do;
			if old_no_sw_plan lt no_sw_plan then no_sw_plan=old_no_sw_plan;end;
		else no_sw_plan=old_no_sw_plan;end;

/* drop old dates */
	drop old_a_sw_plan old_b_sw_plan old_co_sw_plan old_fa_sw_plan old_sm_sw_plan old_no_sw_plan;

	output pshop.multi_output;
	end;

/* delete resigned or sold customers from working copy */
if change = "re-sign/sold" then delete;
/* deleting that row will leave singles which should also be deleted */
/* this will need to be done in the next step */
output work.look_at_multis;

run;

*** distill the new or lapsed return customers ;
data work.second_step_multis (drop=new_addr start_date end_date checking); /* work.letscheck;*/
set work.look_at_multis;
by unique_cust start_date;

/* remove the initial row from the re-sign - sold customers in the previous step */
if first.unique_cust and last.unique_cust then do;
	/*output work.letscheck;*/ /* 40 missing - small error variable - >2 in the multi contracts */
	delete;
	end;

/* rest the start and end dates for customers first entry */
if first.unique_cust then do;
	start_work = start_date;
	end_work = end_date;
/* retains the change = "new" as this is the customers first row */
	end;

checking = lag(start_date);
format checking date9.;

/* distinguish between the new in their own right and lapsed return customers */
if not first.unique_cust then do;
	if start_date = checking then do;
		start_work = start_date;
		end_work = end_date;
		change = "new"; /* new first customer contract row */
		end;
	else do;
		start_work = start_date;
		end_work = end_date;
		change = "lapsed return"; /* returned customer outside of the &custen period */
		end;
	end;

run;

*** append the work.second_step_multis dataset onto pshop.multi_output;
proc append 	base=pshop.multi_output 
				data=work.second_step_multis appendver=v6;
run;
proc sort data=pshop.multi_output;by unique_cust start_work;run;

*** append the pshop.cust_single dataset onto pshop.multi_output;
proc append 	base=pshop.multi_output 
				data=pshop.cust_single appendver=v6;
run;
proc sort 	data=pshop.multi_output
			out=pshop.customer_tenure;
			by unique_cust start_work;
run;

***************************************************************************************************;
*** create repeated measure dataset by unique_cust, custid and consid ;
***************************************************************************************************;
*** first - when does this data begin;
proc sql;
select
	min(start_work) format date9.
into :first_ctrt
from
	pshop.customer_tenure
;	
%let first_ctrt=&first_ctrt;
%put NOTE: earliest customer start date is &first_ctrt ; 

*** calculate the number of months to go back from today to 17 November 2007 ;
data _null_;
	call symput("mth_back",intck('month',"&first_ctrt"d,&monthend.)+1);
run;
*readout macros for easy reminding access ;
%put &mth_back;

***************************************************************************************************;
*** the generic time variable going forward will be monthly;
*** I am treating this data as discrete so what happens in a month does not allow for days of the month ;

*** second loop through each month from the first reporting month to create a timeseries datasets;
%dt_monthend(&mth_back.);

%macro rep_data(start);
	%do i=&start %to 1 %by -1;
		%if &i = &mth_back %then %do;
			/* collect active month information and exit month information */
			data work.repeat_measure&i;
				set pshop.customer_tenure;
				where &monthend. between 	intnx('month',start_work,0,'e') and 
											intnx('month',end_work,0,'e');
				ten_main = cat('active_month',intck('month',start_work,&monthend.));
				exist = 1;
				if intnx('month',end_work,0,'e') eq &monthend. then 
					exit=cat('exit_month',intck('month',start_work,&monthend.));
			run;
			/*	want counts of customers active at x months or exited at x months
				so create columns using proc transpose */
			%macro transdta(varname,colname);
				proc sort data=work.repeat_measure&i;by unique_cust &varname;run;

				proc transpose 	data=work.repeat_measure&i
								out=rm_trans_&varname (drop=_name_ ); 
				by unique_cust;
				id &varname;
				var exist;
				run;
			%mend;
			%transdta(ten_main,active_mth);
			%transdta(exit,exit_mth);
			/* final dataset starts here - subsequent months will be appended */
			data pshop.repeat_measure;
			merge 	work.rm_trans_ten_main(in=a)
					work.rm_trans_exit(in=b);
			if a and b;
			run;	
			proc sort data=pshop.repeat_measure;by unique_cust;run;
			/* tidy out the monthly increment dataset */
			proc datasets library=work;
				delete repeat_measure&i;
			run;
		%end;
		%else %do;
			%dt_monthend(&i);
			data work.repeat_measure&i;
				set pshop.customer_tenure;
				where &monthend. between 	intnx('month',start_work,0,'e') and 
											intnx('month',end_work,0,'e');
				ten_main = cat('active_month',intck('month',start_work,&monthend.));
				exist = 1;
				if intnx('month',end_work,0,'e') eq &monthend. then 
					exit=cat('exit_month',intck('month',start_work,&monthend.));
			run;
			/*	subsequent customers active at x months or exited at x months */
			%macro transdta(varname,colname);
				proc sort data=work.repeat_measure&i;by unique_cust &varname;run;

				proc transpose 	data=work.repeat_measure&i
								out=rm_trans_&varname (drop=_name_ ); 
				by unique_cust;
				id &varname;
				var exist;
				run;
			%mend;
			%transdta(ten_main,active_mth);
			%transdta(exit,exit_mth);
			/* prepare subsequent month(s) data to be appended */
			data work.repeat_measure_hold;
			merge 	work.rm_trans_ten_main(in=a)
					work.rm_trans_exit(in=b);
			if a and b;
			run;
			proc sort data=work.repeat_measure_hold;by unique_cust;run;
			/* append data for final analysis */
			data pshop.repeat_measure;
			update 	pshop.repeat_measure(in=a)
					work.repeat_measure_hold(in=b);
			by unique_cust;
			run;
			proc sort data=pshop.repeat_measure;by unique_cust;run;
			/* tidy out the monthly increment dataset */
			proc datasets library=work;
				delete repeat_measure&i;
			run;
		%end;
	%end;
%mend;
%rep_data(&mth_back);

%dt_monthend(1);
***************************************************************************************************;
/* ran above for i from 95 to 83 for the first year */
/*
proc sql;
create table pshop.year1 as
select
	'30nov2008'd as month format date9.,
	count(active_month0) as month0,
	count(exit_month0) as exit0,
	count(active_month1) as month1,
	count(exit_month1) as exit1,
	count(active_month2) as month2,
	count(exit_month2) as exit2,
	count(active_month3) as month3,
	count(exit_month3) as exit3,
	count(active_month4) as month4,
	count(exit_month4) as exit4,
	count(active_month5) as month5,
	count(exit_month5) as exit5,
	count(active_month6) as month6,
	count(exit_month6) as exit6,
	count(active_month7) as month7,
	count(exit_month7) as exit7,
	count(active_month8) as month8,
	count(exit_month8) as exit8,
	count(active_month9) as month9,
	count(exit_month9) as exit9,
	count(active_month10) as month10,
	count(exit_month10) as exit10,
	count(active_month11) as month11,
	count(exit_month11) as exit11,
	count(active_month12) as month12
from
	pshop.repeat_measure
;
quit;
*** calculate tenure after year one ;
%dt_monthend(83);
data work.how_old_one_yr;
set pshop.customer_tenure;
where &monthend. between 	intnx('month',start_work,0,'e') and 
							intnx('month',end_work,0,'e');
tenure = intck('month',start_work,&monthend.);
run;
*** calculate mean and median tenure after one year;
*** altogether ;
proc sql;
create table pshop.med_mean_ten_main as
select
	&monthend. as month format date9.,
	median(tenure) as median,
	mean(tenure) as mean,
	sqrt(var(tenure)) as stddev
from
	work.how_old_one_yr
;
quit;
*** by plan ;
proc sql;
create table pshop.med_mean_ten_plan as
select
	&monthend. as month format date9.,
	b.sw_plan as sw_plan,
	median(a.tenure) as median,
	mean(a.tenure) as mean,
	sqrt(var(a.tenure)) as stddev 
from
	work.how_old_one_yr a,
	pshop.allcust_mod b
where
	a.unique_cust = cat(b.custid,b.consid,substrn(compress(upcase(b.addr)),1,7))
group by b.sw_plan
;
quit;
%dt_monthend(1);
***************************************************************************************************;
*/
/* ran above for i from 95 to 71 for the second year */
/*
proc sql;
create table pshop.year2 as
select
	'30nov2009'd as month format date9.,
	count(active_month0) as month0,
	count(exit_month0) as exit0,
	count(active_month1) as month1,
	count(exit_month1) as exit1,
	count(active_month2) as month2,
	count(exit_month2) as exit2,
	count(active_month3) as month3,
	count(exit_month3) as exit3,
	count(active_month4) as month4,
	count(exit_month4) as exit4,
	count(active_month5) as month5,
	count(exit_month5) as exit5,
	count(active_month6) as month6,
	count(exit_month6) as exit6,
	count(active_month7) as month7,
	count(exit_month7) as exit7,
	count(active_month8) as month8,
	count(exit_month8) as exit8,
	count(active_month9) as month9,
	count(exit_month9) as exit9,
	count(active_month10) as month10,
	count(exit_month10) as exit10,
	count(active_month11) as month11,
	count(exit_month11) as exit11,
	count(active_month12) as month12,
	count(exit_month12) as exit12,
	count(active_month13) as month13,
	count(active_month14) as month14,
	count(exit_month14) as exit14,
	count(active_month15) as month15,
	count(active_month16) as month16,
	count(exit_month16) as exit16,
	count(active_month17) as month17,
	count(active_month18) as month18,
	count(active_month19) as month19,
	count(active_month20) as month20,
	count(active_month21) as month21,
	count(active_month22) as month22,
	count(active_month23) as month23,
	count(active_month24) as month24
from
	pshop.repeat_measure
;
quit;
*** calculate tenure after year two ;
%dt_monthend(71);
data work.how_old_two_yr;
set pshop.customer_tenure;
where &monthend. between 	intnx('month',start_work,0,'e') and 
							intnx('month',end_work,0,'e');
tenure = intck('month',start_work,&monthend.);
run;
*** calculate mean and median tenure after two year;
proc sql;
create table work.med_mean_ten_main as
select
	&monthend. as month format date9.,
	median(tenure) as median,
	mean(tenure) as mean,
	sqrt(var(tenure)) as stddev
from
	work.how_old_two_yr
;
quit;
data pshop.med_mean_ten_main;
update 	pshop.med_mean_ten_main(in=a)
		work.med_mean_ten_main(in=b);
by month;
run;
*** by plan ;
proc sql;
create table work.med_mean_ten_plan as
select
	&monthend. as month format date9.,
	b.sw_plan as sw_plan,
	median(a.tenure) as median,
	mean(a.tenure) as mean,
	sqrt(var(a.tenure)) as stddev 
from
	work.how_old_two_yr a,
	pshop.allcust_mod b
where
	a.unique_cust = cat(b.custid,b.consid,substrn(compress(upcase(b.addr)),1,7))
group by b.sw_plan
;
quit;
*** append to bottom of dataset;
data pshop.med_mean_ten_plan;
set 	pshop.med_mean_ten_plan(in=a)
		work.med_mean_ten_plan(in=b);
by month;
run;
%dt_monthend(1);
***************************************************************************************************;
*/
/* ran above for i from 95 to 59 for the third year */
/*
proc sql;
create table pshop.year3 as
select
	'30nov2010'd as month format date9.,
	count(active_month0) as month0,
	count(exit_month0) as exit0,
	count(active_month1) as month1,
	count(exit_month1) as exit1,
	count(active_month2) as month2,
	count(exit_month2) as exit2,
	count(active_month3) as month3,
	count(exit_month3) as exit3,
	count(active_month4) as month4,
	count(exit_month4) as exit4,
	count(active_month5) as month5,
	count(exit_month5) as exit5,
	count(active_month6) as month6,
	count(exit_month6) as exit6,
	count(active_month7) as month7,
	count(exit_month7) as exit7,
	count(active_month8) as month8,
	count(exit_month8) as exit8,
	count(active_month9) as month9,
	count(exit_month9) as exit9,
	count(active_month10) as month10,
	count(exit_month10) as exit10,
	count(active_month11) as month11,
	count(exit_month11) as exit11,
	count(active_month12) as month12,
	count(exit_month12) as exit12,
	count(active_month13) as month13,
	count(exit_month13) as exit13,
	count(active_month14) as month14,
	count(exit_month14) as exit14,
	count(active_month15) as month15,
	count(exit_month15) as exit15,
	count(active_month16) as month16,
	count(exit_month16) as exit16,
	count(active_month17) as month17,
	count(exit_month17) as exit17,
	count(active_month18) as month18,
	count(exit_month18) as exit18,
	count(active_month19) as month19,
	count(exit_month19) as exit19,
	count(active_month20) as month20,
	count(exit_month20) as exit20,
	count(active_month21) as month21,
	count(exit_month21) as exit21,
	count(active_month22) as month22,
	count(active_month23) as month23,
	count(exit_month23) as exit23,
	count(active_month24) as month24,
	count(active_month25) as month25,
	count(exit_month25) as exit25,
	count(active_month26) as month26,
	count(active_month27) as month27,
	count(active_month28) as month28,
	count(active_month29) as month29,
	count(active_month30) as month30,
	count(active_month31) as month31,
	count(exit_month31) as exit31,
	count(active_month32) as month32,
	count(active_month33) as month33,
	count(active_month34) as month34,
	count(active_month35) as month35,
	count(active_month36) as month36
from
	pshop.repeat_measure
;
quit;
*** calculate tenure after year three ;
%dt_monthend(59);
data work.how_old_thr_yr;
set pshop.customer_tenure;
where &monthend. between 	intnx('month',start_work,0,'e') and 
							intnx('month',end_work,0,'e');
tenure = intck('month',start_work,&monthend.);
run;
*** calculate mean and median tenure after three year;
proc sql;
create table work.med_mean_ten_main as
select
	&monthend. as month format date9.,
	median(tenure) as median,
	mean(tenure) as mean,
	sqrt(var(tenure)) as stddev
from
	work.how_old_thr_yr
;
quit;
data pshop.med_mean_ten_main;
update 	pshop.med_mean_ten_main(in=a)
		work.med_mean_ten_main(in=b);
by month;
run;
*** by plan ;
proc sql;
create table work.med_mean_ten_plan as
select
	&monthend. as month format date9.,
	b.sw_plan as sw_plan,
	median(a.tenure) as median,
	mean(a.tenure) as mean,
	sqrt(var(a.tenure)) as stddev 
from
	work.how_old_thr_yr a,
	pshop.allcust_mod b
where
	a.unique_cust = cat(b.custid,b.consid,substrn(compress(upcase(b.addr)),1,7))
group by b.sw_plan
;
quit;
*** append to bottom of dataset;
data pshop.med_mean_ten_plan;
set 	pshop.med_mean_ten_plan(in=a)
		work.med_mean_ten_plan(in=b);
by month;
run;
%dt_monthend(1);
***************************************************************************************************;
*/
/* ran above for i from 95 to 47 for the fourth year */
/*
proc sql;
create table pshop.year4 as
select
	'30nov2011'd as month format date9.,
	count(active_month0) as month0,
	count(exit_month0) as exit0,
	count(active_month1) as month1,
	count(exit_month1) as exit1,
	count(active_month2) as month2,
	count(exit_month2) as exit2,
	count(active_month3) as month3,
	count(exit_month3) as exit3,
	count(active_month4) as month4,
	count(exit_month4) as exit4,
	count(active_month5) as month5,
	count(exit_month5) as exit5,
	count(active_month6) as month6,
	count(exit_month6) as exit6,
	count(active_month7) as month7,
	count(exit_month7) as exit7,
	count(active_month8) as month8,
	count(exit_month8) as exit8,
	count(active_month9) as month9,
	count(exit_month9) as exit9,
	count(active_month10) as month10,
	count(exit_month10) as exit10,
	count(active_month11) as month11,
	count(exit_month11) as exit11,
	count(active_month12) as month12,
	count(exit_month12) as exit12,
	count(active_month13) as month13,
	count(exit_month13) as exit13,
	count(active_month14) as month14,
	count(exit_month14) as exit14,
	count(active_month15) as month15,
	count(exit_month15) as exit15,
	count(active_month16) as month16,
	count(exit_month16) as exit16,
	count(active_month17) as month17,
	count(exit_month17) as exit17,
	count(active_month18) as month18,
	count(exit_month18) as exit18,
	count(active_month19) as month19,
	count(exit_month19) as exit19,
	count(active_month20) as month20,
	count(exit_month20) as exit20,
	count(active_month21) as month21,
	count(exit_month21) as exit21,
	count(active_month22) as month22,
	count(exit_month22) as exit22,
	count(active_month23) as month23,
	count(exit_month23) as exit23,
	count(active_month24) as month24,
	count(exit_month24) as exit24,
	count(active_month25) as month25,
	count(exit_month25) as exit25,
	count(active_month26) as month26,
	count(exit_month26) as exit26,
	count(active_month27) as month27,
	count(exit_month27) as exit27,
	count(active_month28) as month28,
	count(exit_month28) as exit28,
	count(active_month29) as month29,
	count(exit_month29) as exit29,
	count(active_month30) as month30,
	count(exit_month30) as exit30,
	count(active_month31) as month31,
	count(exit_month31) as exit31,
	count(active_month32) as month32,
	count(exit_month32) as exit32,
	count(active_month33) as month33,
	count(exit_month33) as exit33,
	count(active_month34) as month34,
	count(active_month35) as month35,
	count(exit_month35) as exit35,
	count(active_month36) as month36,
	count(active_month37) as month37,
	count(active_month38) as month38,
	count(exit_month38) as exit38,
	count(active_month39) as month39,
	count(active_month40) as month40,
	count(exit_month40) as exit40,
	count(active_month41) as month41,
	count(active_month42) as month42,
	count(active_month43) as month43,
	count(active_month44) as month44,
	count(active_month45) as month45,
	count(active_month46) as month46,
	count(active_month47) as month47,
	count(active_month48) as month48
from
	pshop.repeat_measure
;
quit;
*** calculate tenure after year four ;
%dt_monthend(47);
data work.how_old_four_yr;
set pshop.customer_tenure;
where &monthend. between 	intnx('month',start_work,0,'e') and 
							intnx('month',end_work,0,'e');
tenure = intck('month',start_work,&monthend.);
run;
*** calculate mean and median tenure after four year;
proc sql;
create table work.med_mean_ten_main as
select
	&monthend. as month format date9.,
	median(tenure) as median,
	mean(tenure) as mean,
	sqrt(var(tenure)) as stddev
from
	work.how_old_four_yr
;
quit;
data pshop.med_mean_ten_main;
update 	pshop.med_mean_ten_main(in=a)
		work.med_mean_ten_main(in=b);
by month;
run;
*** by plan ;
proc sql;
create table work.med_mean_ten_plan as
select
	&monthend. as month format date9.,
	b.sw_plan as sw_plan,
	median(a.tenure) as median,
	mean(a.tenure) as mean,
	sqrt(var(a.tenure)) as stddev 
from
	work.how_old_four_yr a,
	pshop.allcust_mod b
where
	a.unique_cust = cat(b.custid,b.consid,substrn(compress(upcase(b.addr)),1,7))
group by b.sw_plan
;
quit;
*** append to bottom of dataset;
data pshop.med_mean_ten_plan;
set 	pshop.med_mean_ten_plan(in=a)
		work.med_mean_ten_plan(in=b);
by month;
run;
%dt_monthend(1);
***************************************************************************************************;
*/
/* ran above for i from 95 to 35 for the fifth year */
/*
proc sql;
create table pshop.year5 as
select
	'30nov2012'd as month format date9.,
	count(active_month0) as month0,
	count(exit_month0) as exit0,
	count(active_month1) as month1,
	count(exit_month1) as exit1,
	count(active_month2) as month2,
	count(exit_month2) as exit2,
	count(active_month3) as month3,
	count(exit_month3) as exit3,
	count(active_month4) as month4,
	count(exit_month4) as exit4,
	count(active_month5) as month5,
	count(exit_month5) as exit5,
	count(active_month6) as month6,
	count(exit_month6) as exit6,
	count(active_month7) as month7,
	count(exit_month7) as exit7,
	count(active_month8) as month8,
	count(exit_month8) as exit8,
	count(active_month9) as month9,
	count(exit_month9) as exit9,
	count(active_month10) as month10,
	count(exit_month10) as exit10,
	count(active_month11) as month11,
	count(exit_month11) as exit11,
	count(active_month12) as month12,
	count(exit_month12) as exit12,
	count(active_month13) as month13,
	count(exit_month13) as exit13,
	count(active_month14) as month14,
	count(exit_month14) as exit14,
	count(active_month15) as month15,
	count(exit_month15) as exit15,
	count(active_month16) as month16,
	count(exit_month16) as exit16,
	count(active_month17) as month17,
	count(exit_month17) as exit17,
	count(active_month18) as month18,
	count(exit_month18) as exit18,
	count(active_month19) as month19,
	count(exit_month19) as exit19,
	count(active_month20) as month20,
	count(exit_month20) as exit20,
	count(active_month21) as month21,
	count(exit_month21) as exit21,
	count(active_month22) as month22,
	count(exit_month22) as exit22,
	count(active_month23) as month23,
	count(exit_month23) as exit23,
	count(active_month24) as month24,
	count(exit_month24) as exit24,
	count(active_month25) as month25,
	count(exit_month25) as exit25,
	count(active_month26) as month26,
	count(exit_month26) as exit26,
	count(active_month27) as month27,
	count(exit_month27) as exit27,
	count(active_month28) as month28,
	count(exit_month28) as exit28,
	count(active_month29) as month29,
	count(exit_month29) as exit29,
	count(active_month30) as month30,
	count(exit_month30) as exit30,
	count(active_month31) as month31,
	count(exit_month31) as exit31,
	count(active_month32) as month32,
	count(exit_month32) as exit32,
	count(active_month33) as month33,
	count(exit_month33) as exit33,
	count(active_month34) as month34,
	count(exit_month34) as exit34,
	count(active_month35) as month35,
	count(exit_month35) as exit35,
	count(active_month36) as month36,
	count(exit_month36) as exit36,
	count(active_month37) as month37,
	count(exit_month37) as exit37,
	count(active_month38) as month38,
	count(exit_month38) as exit38,
	count(active_month39) as month39,
	count(exit_month39) as exit39,
	count(active_month40) as month40,
	count(exit_month40) as exit40,
	count(active_month41) as month41,
	count(exit_month41) as exit41,
	count(active_month42) as month42,
	count(exit_month42) as exit42,
	count(active_month43) as month43,
	count(exit_month43) as exit43,
	count(active_month44) as month44,
	count(exit_month44) as exit44,
	count(active_month45) as month45,
	count(active_month46) as month46,
	count(active_month47) as month47,
	count(active_month48) as month48,
	count(active_month49) as month49,
	count(active_month50) as month50,
	count(active_month51) as month51,
	count(exit_month51) as exit51,
	count(active_month52) as month52,
	count(active_month53) as month53,
	count(active_month54) as month54,
	count(active_month55) as month55,
	count(active_month56) as month56,
	count(active_month57) as month57,
	count(active_month58) as month58,
	count(active_month59) as month59,
	count(active_month60) as month60
from
	pshop.repeat_measure
;
quit;
*** calculate tenure after year five ;
%dt_monthend(35);
data work.how_old_five_yr;
set pshop.customer_tenure;
where &monthend. between 	intnx('month',start_work,0,'e') and 
							intnx('month',end_work,0,'e');
tenure = intck('month',start_work,&monthend.);
run;
*** calculate mean and median tenure after five year;
proc sql;
create table work.med_mean_ten_main as
select
	&monthend. as month format date9.,
	median(tenure) as median,
	mean(tenure) as mean,
	sqrt(var(tenure)) as stddev
from
	work.how_old_five_yr
;
quit;
data pshop.med_mean_ten_main;
update 	pshop.med_mean_ten_main(in=a)
		work.med_mean_ten_main(in=b);
by month;
run;
*** by plan ;
proc sql;
create table work.med_mean_ten_plan as
select
	&monthend. as month format date9.,
	b.sw_plan as sw_plan,
	median(a.tenure) as median,
	mean(a.tenure) as mean,
	sqrt(var(a.tenure)) as stddev 
from
	work.how_old_five_yr a,
	pshop.allcust_mod b
where
	a.unique_cust = cat(b.custid,b.consid,substrn(compress(upcase(b.addr)),1,7))
group by b.sw_plan
;
quit;
*** append to bottom of dataset;
data pshop.med_mean_ten_plan;
set 	pshop.med_mean_ten_plan(in=a)
		work.med_mean_ten_plan(in=b);
by month;
run;
%dt_monthend(1);
***************************************************************************************************;
*/
/* ran above for i from 95 to 23 for the sixth year */
/*
proc sql;
create table pshop.year6 as
select
	'30nov2013'd as month format date9.,
	count(active_month0) as month0,
	count(exit_month0) as exit0,
	count(active_month1) as month1,
	count(exit_month1) as exit1,
	count(active_month2) as month2,
	count(exit_month2) as exit2,
	count(active_month3) as month3,
	count(exit_month3) as exit3,
	count(active_month4) as month4,
	count(exit_month4) as exit4,
	count(active_month5) as month5,
	count(exit_month5) as exit5,
	count(active_month6) as month6,
	count(exit_month6) as exit6,
	count(active_month7) as month7,
	count(exit_month7) as exit7,
	count(active_month8) as month8,
	count(exit_month8) as exit8,
	count(active_month9) as month9,
	count(exit_month9) as exit9,
	count(active_month10) as month10,
	count(exit_month10) as exit10,
	count(active_month11) as month11,
	count(exit_month11) as exit11,
	count(active_month12) as month12,
	count(exit_month12) as exit12,
	count(active_month13) as month13,
	count(exit_month13) as exit13,
	count(active_month14) as month14,
	count(exit_month14) as exit14,
	count(active_month15) as month15,
	count(exit_month15) as exit15,
	count(active_month16) as month16,
	count(exit_month16) as exit16,
	count(active_month17) as month17,
	count(exit_month17) as exit17,
	count(active_month18) as month18,
	count(exit_month18) as exit18,
	count(active_month19) as month19,
	count(exit_month19) as exit19,
	count(active_month20) as month20,
	count(exit_month20) as exit20,
	count(active_month21) as month21,
	count(exit_month21) as exit21,
	count(active_month22) as month22,
	count(exit_month22) as exit22,
	count(active_month23) as month23,
	count(exit_month23) as exit23,
	count(active_month24) as month24,
	count(exit_month24) as exit24,
	count(active_month25) as month25,
	count(exit_month25) as exit25,
	count(active_month26) as month26,
	count(exit_month26) as exit26,
	count(active_month27) as month27,
	count(exit_month27) as exit27,
	count(active_month28) as month28,
	count(exit_month28) as exit28,
	count(active_month29) as month29,
	count(exit_month29) as exit29,
	count(active_month30) as month30,
	count(exit_month30) as exit30,
	count(active_month31) as month31,
	count(exit_month31) as exit31,
	count(active_month32) as month32,
	count(exit_month32) as exit32,
	count(active_month33) as month33,
	count(exit_month33) as exit33,
	count(active_month34) as month34,
	count(exit_month34) as exit34,
	count(active_month35) as month35,
	count(exit_month35) as exit35,
	count(active_month36) as month36,
	count(exit_month36) as exit36,
	count(active_month37) as month37,
	count(exit_month37) as exit37,
	count(active_month38) as month38,
	count(exit_month38) as exit38,
	count(active_month39) as month39,
	count(exit_month39) as exit39,
	count(active_month40) as month40,
	count(exit_month40) as exit40,
	count(active_month41) as month41,
	count(exit_month41) as exit41,
	count(active_month42) as month42,
	count(exit_month42) as exit42,
	count(active_month43) as month43,
	count(exit_month43) as exit43,
	count(active_month44) as month44,
	count(exit_month44) as exit44,
	count(active_month45) as month45,
	count(exit_month45) as exit45,
	count(active_month46) as month46,
	count(exit_month46) as exit46,
	count(active_month47) as month47,
	count(exit_month47) as exit47,
	count(active_month48) as month48,
	count(exit_month48) as exit48,
	count(active_month49) as month49,
	count(exit_month49) as exit49,
	count(active_month50) as month50,
	count(exit_month50) as exit50,
	count(active_month51) as month51,
	count(exit_month51) as exit51,
	count(active_month52) as month52,
	count(exit_month52) as exit52,
	count(active_month53) as month53,
	count(exit_month53) as exit53,
	count(active_month54) as month54,
	count(exit_month54) as exit54,
	count(active_month55) as month55,
	count(exit_month55) as exit55,
	count(active_month56) as month56,
	count(exit_month56) as exit56,
	count(active_month57) as month57,
	count(active_month58) as month58,
	count(active_month59) as month59,
	count(active_month60) as month60,
	count(exit_month60) as exit60,
	count(active_month61) as month61,
	count(active_month62) as month62,
	count(exit_month62) as exit62,
	count(active_month63) as month63,
	count(active_month64) as month64,
	count(active_month65) as month65,
	count(exit_month65) as exit65,
	count(active_month66) as month66,
	count(active_month67) as month67,
	count(active_month68) as month68,
	count(active_month69) as month69,
	count(exit_month69) as exit69,
	count(active_month70) as month70,
	count(active_month71) as month71,
	count(active_month72) as month72
from
	pshop.repeat_measure
;
quit;
*** calculate tenure after year six ;
%dt_monthend(23);
data work.how_old_six_yr;
set pshop.customer_tenure;
where &monthend. between 	intnx('month',start_work,0,'e') and 
							intnx('month',end_work,0,'e');
tenure = intck('month',start_work,&monthend.);
run;
*** calculate mean and median tenure after six year;
proc sql;
create table work.med_mean_ten_main as
select
	&monthend. as month format date9.,
	median(tenure) as median,
	mean(tenure) as mean,
	sqrt(var(tenure)) as stddev
from
	work.how_old_six_yr
;
quit;
data pshop.med_mean_ten_main;
update 	pshop.med_mean_ten_main(in=a)
		work.med_mean_ten_main(in=b);
by month;
run;
*** by plan ;
proc sql;
create table work.med_mean_ten_plan as
select
	&monthend. as month format date9.,
	b.sw_plan as sw_plan,
	median(a.tenure) as median,
	mean(a.tenure) as mean,
	sqrt(var(a.tenure)) as stddev 
from
	work.how_old_six_yr a,
	pshop.allcust_mod b
where
	a.unique_cust = cat(b.custid,b.consid,substrn(compress(upcase(b.addr)),1,7))
group by b.sw_plan
;
quit;
*** append to bottom of dataset;
data pshop.med_mean_ten_plan;
set 	pshop.med_mean_ten_plan(in=a)
		work.med_mean_ten_plan(in=b);
by month;
run;
%dt_monthend(1);
***************************************************************************************************;
*/
/* ran above for i from 95 to 11 for the seventh year */
/*
proc sql;
create table pshop.year7 as
select
	'30nov2014'd as month format date9.,
	count(active_month0) as month0,
	count(exit_month0) as exit0,
	count(active_month1) as month1,
	count(exit_month1) as exit1,
	count(active_month2) as month2,
	count(exit_month2) as exit2,
	count(active_month3) as month3,
	count(exit_month3) as exit3,
	count(active_month4) as month4,
	count(exit_month4) as exit4,
	count(active_month5) as month5,
	count(exit_month5) as exit5,
	count(active_month6) as month6,
	count(exit_month6) as exit6,
	count(active_month7) as month7,
	count(exit_month7) as exit7,
	count(active_month8) as month8,
	count(exit_month8) as exit8,
	count(active_month9) as month9,
	count(exit_month9) as exit9,
	count(active_month10) as month10,
	count(exit_month10) as exit10,
	count(active_month11) as month11,
	count(exit_month11) as exit11,
	count(active_month12) as month12,
	count(exit_month12) as exit12,
	count(active_month13) as month13,
	count(exit_month13) as exit13,
	count(active_month14) as month14,
	count(exit_month14) as exit14,
	count(active_month15) as month15,
	count(exit_month15) as exit15,
	count(active_month16) as month16,
	count(exit_month16) as exit16,
	count(active_month17) as month17,
	count(exit_month17) as exit17,
	count(active_month18) as month18,
	count(exit_month18) as exit18,
	count(active_month19) as month19,
	count(exit_month19) as exit19,
	count(active_month20) as month20,
	count(exit_month20) as exit20,
	count(active_month21) as month21,
	count(exit_month21) as exit21,
	count(active_month22) as month22,
	count(exit_month22) as exit22,
	count(active_month23) as month23,
	count(exit_month23) as exit23,
	count(active_month24) as month24,
	count(exit_month24) as exit24,
	count(active_month25) as month25,
	count(exit_month25) as exit25,
	count(active_month26) as month26,
	count(exit_month26) as exit26,
	count(active_month27) as month27,
	count(exit_month27) as exit27,
	count(active_month28) as month28,
	count(exit_month28) as exit28,
	count(active_month29) as month29,
	count(exit_month29) as exit29,
	count(active_month30) as month30,
	count(exit_month30) as exit30,
	count(active_month31) as month31,
	count(exit_month31) as exit31,
	count(active_month32) as month32,
	count(exit_month32) as exit32,
	count(active_month33) as month33,
	count(exit_month33) as exit33,
	count(active_month34) as month34,
	count(exit_month34) as exit34,
	count(active_month35) as month35,
	count(exit_month35) as exit35,
	count(active_month36) as month36,
	count(exit_month36) as exit36,
	count(active_month37) as month37,
	count(exit_month37) as exit37,
	count(active_month38) as month38,
	count(exit_month38) as exit38,
	count(active_month39) as month39,
	count(exit_month39) as exit39,
	count(active_month40) as month40,
	count(exit_month40) as exit40,
	count(active_month41) as month41,
	count(exit_month41) as exit41,
	count(active_month42) as month42,
	count(exit_month42) as exit42,
	count(active_month43) as month43,
	count(exit_month43) as exit43,
	count(active_month44) as month44,
	count(exit_month44) as exit44,
	count(active_month45) as month45,
	count(exit_month45) as exit45,
	count(active_month46) as month46,
	count(exit_month46) as exit46,
	count(active_month47) as month47,
	count(exit_month47) as exit47,
	count(active_month48) as month48,
	count(exit_month48) as exit48,
	count(active_month49) as month49,
	count(exit_month49) as exit49,
	count(active_month50) as month50,
	count(exit_month50) as exit50,
	count(active_month51) as month51,
	count(exit_month51) as exit51,
	count(active_month52) as month52,
	count(exit_month52) as exit52,
	count(active_month53) as month53,
	count(exit_month53) as exit53,
	count(active_month54) as month54,
	count(exit_month54) as exit54,
	count(active_month55) as month55,
	count(exit_month55) as exit55,
	count(active_month56) as month56,
	count(exit_month56) as exit56,
	count(active_month57) as month57,
	count(exit_month57) as exit57,
	count(active_month58) as month58,
	count(exit_month58) as exit58,
	count(active_month59) as month59,
	count(exit_month59) as exit59,
	count(active_month60) as month60,
	count(exit_month60) as exit60,
	count(active_month61) as month61,
	count(exit_month61) as exit61,
	count(active_month62) as month62,
	count(exit_month62) as exit62,
	count(active_month63) as month63,
	count(exit_month63) as exit63,
	count(active_month64) as month64,
	count(exit_month64) as exit64,
	count(active_month65) as month65,
	count(exit_month65) as exit65,
	count(active_month66) as month66,
	count(exit_month66) as exit66,
	count(active_month67) as month67,
	count(exit_month67) as exit67,
	count(active_month68) as month68,
	count(active_month69) as month69,
	count(exit_month69) as exit69,
	count(active_month70) as month70,
	count(active_month71) as month71,
	count(exit_month71) as exit71,
	count(active_month72) as month72,
	count(active_month73) as month73,
	count(active_month74) as month74,
	count(active_month75) as month75,
	count(active_month76) as month76,
	count(exit_month76) as exit76,
	count(active_month77) as month77,
	count(active_month78) as month78,
	count(exit_month78) as exit78,
	count(active_month79) as month79,
	count(exit_month79) as exit79,
	count(active_month80) as month80,
	count(active_month81) as month81,
	count(active_month82) as month82,
	count(active_month83) as month83,
	count(active_month84) as month84
from
	pshop.repeat_measure
;
quit;
*** calculate tenure after year seven ;
%dt_monthend(11);
data work.how_old_svn_yr;
set pshop.customer_tenure;
where &monthend. between 	intnx('month',start_work,0,'e') and 
							intnx('month',end_work,0,'e');
tenure = intck('month',start_work,&monthend.);
run;
*** calculate mean and median tenure after seven year;
proc sql;
create table work.med_mean_ten_main as
select
	&monthend. as month format date9.,
	median(tenure) as median,
	mean(tenure) as mean,
	sqrt(var(tenure)) as stddev
from
	work.how_old_svn_yr
;
quit;
data pshop.med_mean_ten_main;
update 	pshop.med_mean_ten_main(in=a)
		work.med_mean_ten_main(in=b);
by month;
run;
*** by plan ;
proc sql;
create table work.med_mean_ten_plan as
select
	&monthend. as month format date9.,
	b.sw_plan as sw_plan,
	median(a.tenure) as median,
	mean(a.tenure) as mean,
	sqrt(var(a.tenure)) as stddev 
from
	work.how_old_svn_yr a,
	pshop.allcust_mod b
where
	a.unique_cust = cat(b.custid,b.consid,substrn(compress(upcase(b.addr)),1,7))
group by b.sw_plan
;
quit;
*** append to bottom of dataset;
data pshop.med_mean_ten_plan;
set 	pshop.med_mean_ten_plan(in=a)
		work.med_mean_ten_plan(in=b);
by month;
run;
%dt_monthend(1);
***************************************************************************************************;
*/
/* ran above for i from 95 to 1 for the eighth year */
proc sql;
create table pshop.year8 as
select
	&monthend as month format date9.,
	count(active_month0) as month0,
	count(exit_month0) as exit0,
	count(active_month1) as month1,
	count(exit_month1) as exit1,
	count(active_month2) as month2,
	count(exit_month2) as exit2,
	count(active_month3) as month3,
	count(exit_month3) as exit3,
	count(active_month4) as month4,
	count(exit_month4) as exit4,
	count(active_month5) as month5,
	count(exit_month5) as exit5,
	count(active_month6) as month6,
	count(exit_month6) as exit6,
	count(active_month7) as month7,
	count(exit_month7) as exit7,
	count(active_month8) as month8,
	count(exit_month8) as exit8,
	count(active_month9) as month9,
	count(exit_month9) as exit9,
	count(active_month10) as month10,
	count(exit_month10) as exit10,
	count(active_month11) as month11,
	count(exit_month11) as exit11,
	count(active_month12) as month12,
	count(exit_month12) as exit12,
	count(active_month13) as month13,
	count(exit_month13) as exit13,
	count(active_month14) as month14,
	count(exit_month14) as exit14,
	count(active_month15) as month15,
	count(exit_month15) as exit15,
	count(active_month16) as month16,
	count(exit_month16) as exit16,
	count(active_month17) as month17,
	count(exit_month17) as exit17,
	count(active_month18) as month18,
	count(exit_month18) as exit18,
	count(active_month19) as month19,
	count(exit_month19) as exit19,
	count(active_month20) as month20,
	count(exit_month20) as exit20,
	count(active_month21) as month21,
	count(exit_month21) as exit21,
	count(active_month22) as month22,
	count(exit_month22) as exit22,
	count(active_month23) as month23,
	count(exit_month23) as exit23,
	count(active_month24) as month24,
	count(exit_month24) as exit24,
	count(active_month25) as month25,
	count(exit_month25) as exit25,
	count(active_month26) as month26,
	count(exit_month26) as exit26,
	count(active_month27) as month27,
	count(exit_month27) as exit27,
	count(active_month28) as month28,
	count(exit_month28) as exit28,
	count(active_month29) as month29,
	count(exit_month29) as exit29,
	count(active_month30) as month30,
	count(exit_month30) as exit30,
	count(active_month31) as month31,
	count(exit_month31) as exit31,
	count(active_month32) as month32,
	count(exit_month32) as exit32,
	count(active_month33) as month33,
	count(exit_month33) as exit33,
	count(active_month34) as month34,
	count(exit_month34) as exit34,
	count(active_month35) as month35,
	count(exit_month35) as exit35,
	count(active_month36) as month36,
	count(exit_month36) as exit36,
	count(active_month37) as month37,
	count(exit_month37) as exit37,
	count(active_month38) as month38,
	count(exit_month38) as exit38,
	count(active_month39) as month39,
	count(exit_month39) as exit39,
	count(active_month40) as month40,
	count(exit_month40) as exit40,
	count(active_month41) as month41,
	count(exit_month41) as exit41,
	count(active_month42) as month42,
	count(exit_month42) as exit42,
	count(active_month43) as month43,
	count(exit_month43) as exit43,
	count(active_month44) as month44,
	count(exit_month44) as exit44,
	count(active_month45) as month45,
	count(exit_month45) as exit45,
	count(active_month46) as month46,
	count(exit_month46) as exit46,
	count(active_month47) as month47,
	count(exit_month47) as exit47,
	count(active_month48) as month48,
	count(exit_month48) as exit48,
	count(active_month49) as month49,
	count(exit_month49) as exit49,
	count(active_month50) as month50,
	count(exit_month50) as exit50,
	count(active_month51) as month51,
	count(exit_month51) as exit51,
	count(active_month52) as month52,
	count(exit_month52) as exit52,
	count(active_month53) as month53,
	count(exit_month53) as exit53,
	count(active_month54) as month54,
	count(exit_month54) as exit54,
	count(active_month55) as month55,
	count(exit_month55) as exit55,
	count(active_month56) as month56,
	count(exit_month56) as exit56,
	count(active_month57) as month57,
	count(exit_month57) as exit57,
	count(active_month58) as month58,
	count(exit_month58) as exit58,
	count(active_month59) as month59,
	count(exit_month59) as exit59,
	count(active_month60) as month60,
	count(exit_month60) as exit60,
	count(active_month61) as month61,
	count(exit_month61) as exit61,
	count(active_month62) as month62,
	count(exit_month62) as exit62,
	count(active_month63) as month63,
	count(exit_month63) as exit63,
	count(active_month64) as month64,
	count(exit_month64) as exit64,
	count(active_month65) as month65,
	count(exit_month65) as exit65,
	count(active_month66) as month66,
	count(exit_month66) as exit66,
	count(active_month67) as month67,
	count(exit_month67) as exit67,
	count(active_month68) as month68,
	count(exit_month68) as exit68,
	count(active_month69) as month69,
	count(exit_month69) as exit69,
	count(active_month70) as month70,
	count(exit_month70) as exit70,
	count(active_month71) as month71,
	count(exit_month71) as exit71,	
	count(active_month72) as month72,
	count(exit_month72) as exit72,
	count(active_month73) as month73,
	count(exit_month73) as exit73,
	count(active_month74) as month74,
	count(exit_month74) as exit74,
	count(active_month75) as month75,
	count(exit_month75) as exit75,
	count(active_month76) as month76,
	count(exit_month76) as exit76,
	count(active_month77) as month77,
	count(exit_month77) as exit77,
	count(active_month78) as month78,
	count(exit_month78) as exit78,
	count(active_month79) as month79,
	count(exit_month79) as exit79,
	count(active_month80) as month80,
	count(exit_month80) as exit80,
	count(active_month81) as month81,
	count(exit_month81) as exit81,
	count(active_month82) as month82,
	count(active_month83) as month83,
	count(active_month84) as month84,
	count(active_month85) as month85,
	count(active_month86) as month86,
	count(exit_month86) as exit86,
	count(active_month87) as month87,
	count(active_month88) as month88,
	count(active_month89) as month89,
	count(active_month90) as month90,
	count(exit_month90) as exit90,
	count(active_month91) as month91,
	count(exit_month91) as exit91,
	count(active_month92) as month92,
	count(exit_month92) as exit92,
	count(active_month93) as month93,
	count(exit_month93) as exit93,
	count(active_month94) as month94,
	count(exit_month94) as exit94
from
	pshop.repeat_measure
;
quit;
*** calculate tenure after year eight ;
%dt_monthend(1);
data work.how_old_eight_yr;
set pshop.customer_tenure;
where &monthend. between 	intnx('month',start_work,0,'e') and 
							intnx('month',end_work,0,'e');
tenure = intck('month',start_work,&monthend.);
run;
*** calculate mean and median tenure after year eight;
proc sql;
create table work.med_mean_ten_main as
select
	&monthend. as month format date9.,
	median(tenure) as median,
	mean(tenure) as mean,
	sqrt(var(tenure)) as stddev
from
	work.how_old_eight_yr
;
quit;
data pshop.med_mean_ten_main;
update 	pshop.med_mean_ten_main(in=a)
		work.med_mean_ten_main(in=b);
by month;
run;
*** by plan ;
proc sql;
create table work.med_mean_ten_plan as
select
	&monthend. as month format date9.,
	b.sw_plan as sw_plan,
	median(a.tenure) as median,
	mean(a.tenure) as mean,
	sqrt(var(a.tenure)) as stddev 
from
	work.how_old_eight_yr a,
	pshop.allcust_mod b
where
	a.unique_cust = cat(b.custid,b.consid,substrn(compress(upcase(b.addr)),1,7))
group by b.sw_plan
;
quit;
*** append to bottom of dataset;
data pshop.med_mean_ten_plan;
set 	pshop.med_mean_ten_plan(in=a)
		work.med_mean_ten_plan(in=b);
by month;
run;
%dt_monthend(1);

*** create one dataset with all previous year active and exit tallys as rows;
data pshop.actual_year;
retain 	month month0 exit0 month1 exit1 month2 exit2 month3 exit3 month4 exit4 month5 exit5 month6 exit6
		month7 exit7 month8 exit8 month9 exit9 month10 exit10 month11 exit11 month12 exit12 month13 exit13
		month14 exit14 month15 exit15 month16 exit16 month17 exit17 month18 exit18 month19 exit19 month20
		exit20 month21 exit21 month22 exit22 month23 exit23 month24 exit24 month25 exit25 month26 exit26
		month27 exit27 month28 exit28 month29 exit29 month30 exit30 month31 exit31 month32 exit32 month33
		exit33 month34 exit34 month35 exit35 month36 exit36 month37 exit37 month38 exit38 month39 exit39
		month40 exit40 month41 exit41 month42 exit42 month43 exit43 month44 exit44 month45 exit45 month46
		exit46 month47 exit47 month48 exit48 month49 exit49 month50 exit50 month51 exit51 month52 exit52
		month53 exit53 month54 exit54 month55 exit55 month56 exit56 month57 exit57 month58 exit58 month59
		exit59 month60 exit60 month61 exit61 month62 exit62 month63 exit63 month64 exit64 month65 exit65
		month66 exit66 month67 exit67 month68 exit68 month69 exit69 month70 exit70 month71 exit71 month72
		exit72 month73 exit73 month74 exit74 month75 exit75 month76 exit76 month77 exit77 month78 exit78
		month79 exit79 month80 exit80 month81 exit81 month82 exit82 month83 exit83 month84 exit84 month85 
		exit85 month86 exit86 month87 exit87 month88 exit88 month89 exit89 month90 exit90 month91 exit91 
		month92 exit92 month93 exit93 month94 exit94
		;

set	pshop.year1
	pshop.year2
	pshop.year3
	pshop.year4
	pshop.year5
	pshop.year6
	pshop.year7
	pshop.year8;
run;

*** use following code to create above retain statement ;
/*
proc sql;
create table temp as
select
	name
from
	sashelp.vcolumn
where
	libname="PSHOP"
and memname="ACTUAL_YEAR"
;
quit;
*/
*** calculate the probability of leaving or staying at a given month tenure ;
data pshop.hazard_year;
retain month prob_leave0 prob_stay0 prob_leave1 prob_stay1 prob_leave2 prob_stay2 prob_leave3 prob_stay3
		prob_leave4 prob_stay4 prob_leave5 prob_stay5 prob_leave6 prob_stay6 prob_leave7 prob_stay7 prob_leave8
		prob_stay8 prob_leave9 prob_stay9 prob_leave10 prob_stay10 prob_leave11 prob_stay11 prob_leave12 prob_stay12
		prob_leave13 prob_stay13 prob_leave14 prob_stay14 prob_leave15 prob_stay15 prob_leave16 prob_stay16
		prob_leave17 prob_stay17 prob_leave18 prob_stay18 prob_leave19 prob_stay19 prob_leave20 prob_stay20
		prob_leave21 prob_stay21 prob_leave22 prob_stay22 prob_leave23 prob_stay23 prob_leave24 prob_stay24
		prob_leave25 prob_stay25 prob_leave26 prob_stay26 prob_leave27 prob_stay27 prob_leave28 prob_stay28 
		prob_leave29 prob_stay29 prob_leave30 prob_stay30 prob_leave31 prob_stay31 prob_leave32 prob_stay32
		prob_leave33 prob_stay33 prob_leave34 prob_stay34 prob_leave35 prob_stay35 prob_leave36 prob_stay36
		prob_leave37 prob_stay37 prob_leave38 prob_stay38 prob_leave39 prob_stay39 prob_leave40 prob_stay40
		prob_leave41 prob_stay41 prob_leave42 prob_stay42 prob_leave43 prob_stay43 prob_leave44 prob_stay44
		prob_leave45 prob_stay45 prob_leave46 prob_stay46 prob_leave47 prob_stay47 prob_leave48 prob_stay48
		prob_leave49 prob_stay49 prob_leave50 prob_stay50 prob_leave51 prob_stay51 prob_leave52 prob_stay52
		prob_leave53 prob_stay53 prob_leave54 prob_stay54 prob_leave55 prob_stay55 prob_leave56 prob_stay56
		prob_leave57 prob_stay57 prob_leave58 prob_stay58 prob_leave59 prob_stay59 prob_leave60 prob_stay60
		prob_leave61 prob_stay61 prob_leave62 prob_stay62 prob_leave63 prob_stay63 prob_leave64 prob_stay64
		prob_leave65 prob_stay65 prob_leave66 prob_stay66 prob_leave67 prob_stay67 prob_leave68 prob_stay68
		prob_leave69 prob_stay69 prob_leave70 prob_stay70 prob_leave71 prob_stay71 prob_leave72 prob_stay72
		prob_leave73 prob_stay73 prob_leave74 prob_stay74 prob_leave75 prob_stay75 prob_leave76 prob_stay76
		prob_leave77 prob_stay77 prob_leave78 prob_stay78 prob_leave79 prob_stay79 prob_leave80 prob_stay80
		prob_leave81 prob_stay81 prob_leave82 prob_stay82 prob_leave83 prob_stay83 prob_leave84 prob_stay84
		prob_leave85 prob_stay85 prob_leave86 prob_stay86 prob_leave87 prob_stay87 prob_leave88 prob_stay88
		prob_leave89 prob_stay89 prob_leave90 prob_stay90 prob_leave91 prob_stay91 prob_leave92 prob_stay92
		prob_leave93 prob_stay93 prob_leave94 prob_stay94
		;
set	pshop.actual_year;
by month;

array m{*} month0-month94;
array e{*} exit0-exit94;
array l(*) prob_leave0-prob_leave94;
array s(*) prob_stay0-prob_stay94;
do i = 1 to 95;
	l{i} = e{i}/m{i};
	if m{i} gt 0 then do;
		if l{i} = . then l{i} = 0;
	end;
	s{i} = 1-l{i};
end;
drop i month0 exit0 month1 exit1 month2 exit2 month3 exit3 month4 exit4 month5 exit5 month6 exit6
		month7 exit7 month8 exit8 month9 exit9 month10 exit10 month11 exit11 month12 exit12 month13 exit13
		month14 exit14 month15 exit15 month16 exit16 month17 exit17 month18 exit18 month19 exit19 month20
		exit20 month21 exit21 month22 exit22 month23 exit23 month24 exit24 month25 exit25 month26 exit26
		month27 exit27 month28 exit28 month29 exit29 month30 exit30 month31 exit31 month32 exit32 month33
		exit33 month34 exit34 month35 exit35 month36 exit36 month37 exit37 month38 exit38 month39 exit39
		month40 exit40 month41 exit41 month42 exit42 month43 exit43 month44 exit44 month45 exit45 month46
		exit46 month47 exit47 month48 exit48 month49 exit49 month50 exit50 month51 exit51 month52 exit52
		month53 exit53 month54 exit54 month55 exit55 month56 exit56 month57 exit57 month58 exit58 month59
		exit59 month60 exit60 month61 exit61 month62 exit62 month63 exit63 month64 exit64 month65 exit65
		month66 exit66 month67 exit67 month68 exit68 month69 exit69 month70 exit70 month71 exit71 month72
		exit72 month73 exit73 month74 exit74 month75 exit75 month76 exit76 month77 exit77 month78 exit78
		month79 exit79 month80 exit80 month81 exit81 month82 exit82 month83 exit83 month84 exit84 month85 
		exit85 month86 exit86 month87 exit87 month88 exit88 month89 exit89 month90 exit90 month91 exit91 
		month92 exit92 month93 exit93 month94 exit94;
run;

*** use following code to create above retain statement ;
/*
proc sql;
create table temp as
select
	name
from
	sashelp.vcolumn
where
	libname="PSHOP"
and memname="HAZARD_YEAR"
;
quit;
*/

*** export probability of leaving data ;
data work.leaving_year;
set pshop.hazard_year;
keep month prob_leave0-prob_leave94;
run;

proc export data=work.leaving_year
			outfile="&outdata.\leaving.csv"
			dbms=csv
			replace;
run;
*** export probability of staying data ;
data work.staying_year;
set pshop.hazard_year;
keep month prob_stay0-prob_stay94;
run;

proc export data=work.staying_year
			outfile="&outdata.\staying.csv"
			dbms=csv
			replace;
run;
*** export median and mean of whole population ;
proc export data=pshop.med_mean_ten_main
			outfile="&outdata.\full_med_mean.csv"
			dbms=csv
			replace;
run;
*** export median and mean of sub populations ;
proc export data=pshop.med_mean_ten_plan
			outfile="&outdata.\sub_med_mean.csv"
			dbms=csv
			replace;
run;

***************************************************************************************************;
